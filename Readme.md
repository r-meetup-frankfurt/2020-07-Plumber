
---
title: "R Meetup: Plumb it and ship it"
author: "Daniel Krieg"
date: "07 July 2020"
output: 
  html_document: 
    keep_md: yes
---

# Intro

* Plumber is an R package that allows you to "decorate" existing functions to directly publish them via a REST API
* Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package.

# Step 1: From R functions to REST Api

1. Decorate your existing R functions (or at least a small wrapper around them):


```r
### File: plumber.R

my_cool_function <- function(message) {
  # process
}

#* @get /cool
#* @param message
function(message) {
  # Handle any problems and return useful HTTP error codes in case
  my_cool_function(message)
}
```


2. Run the plumber server


```r
library(plumber)

pr <- plumber::plumb('plumber.R')
pr$run(host='0.0.0.0')
```

3. Test the API via the buildin swagger UI or via curl on the commandline




# Step 2: From local server to docker container

Preparation:

- Install docker
- pull image (e.g. rocker/tidyverse)

Steps:

1. Write a dockerfile

```
FROM rocker/tidyverse:3.6.2
RUN apt-get update -qq && \ 
    apt-get install -y  --no-install-recommends \
    libsodium-dev libcurl4-openssl-dev \ 
    libssl-dev zlib1g-dev dos2unix && \ 
    rm -rf /var/lib/apt/lists/*
    
RUN echo "options(repos = c(CRAN = 'https://cran.uni-muenster.de/'), download.file.method = 'libcurl')" \
    >> /usr/local/lib/R/etc/Rprofile.site
RUN Rscript -e 'install.packages(c("remotes"))'
RUN Rscript -e 'remotes::install_github("rstudio/plumber",upgrade="never", \ 
    ref = "06c32750bf198b8ad4539124c9f68c12cb389f2f")'
RUN Rscript -e 'install.packages("base64enc")'
RUN Rscript -e 'install.packages("httr")'



RUN mkdir -p /var/plumber/app
COPY . /var/plumber/app/
WORKDIR /var/plumber/app

RUN chmod +x plumb.sh
RUN dos2unix plumb.sh

EXPOSE 80
ENTRYPOINT ["./plumb.sh"]
   
CMD ["plumber.R"]

```

2. Build the image 

```
docker build -t pipeline:latest apps/data_pipeline
```

3. Run the container

```
docker run --name pipeline_container --rm -p 8000:80 pipeline:latest
```

4. Remove the container

```
docker rm pipeline_container
```



# Step 3: Querying the REST API from R



```r
library(httr)
library(magrittr)
```





## Process data 

Call the endpoint for the `process_data` function


```r
result_data <- httr::GET(plumber_url("/process"),
                    query=list(user="daniel", file="mtcars.csv", variable="cyl", observable="mpg"))
```

The http response contains a json string:


```r
result_data_json <- result_data %>% 
  httr::content("text", encoding = "UTF-8") 

result_data_json
```

```
## [1] "[{\"cyl\":4,\"mpg\":26.6636},{\"cyl\":6,\"mpg\":19.7429},{\"cyl\":8,\"mpg\":15.1}]"
```

This can be converted to a dataframe via `jsonlite::fromJSON()` method:


```r
result_data_df <- result_data %>% 
  # calls jsonlite::fromJSON()
  httr::content("parsed", simplifyVector = TRUE) %>% 
  tibble::as_tibble()

result_data_df
```

```
## # A tibble: 3 x 2
##     cyl   mpg
##   <int> <dbl>
## 1     4  26.7
## 2     6  19.7
## 3     8  15.1
```



## Plot data 


```r
plot_result <- httr::GET(plumber_url("/plot"),
                         query=list(dataframe=result_data_json, x="cyl", y="mpg"))

png <- plot_result %>% 
  httr::content("raw") %>% 
  png::readPNG()

grid::grid.raster(png)
```

![](Readme_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


## Wrong file



```r
httr::GET(plumber_url("/process"),
          query=list(user="daniel", file="iris.csv", variable="Species", observable="Sepal.Length"))
```

```
## Response [http://localhost:8000/process?user=daniel&file=iris.csv&variable=Species&observable=Sepal.Length]
##   Date: 2020-07-06 20:03
##   Status: 400
##   Content-Type: application/json
##   Size: 62 B
```


## Show available data



```r
httr::GET(plumber_url("/list"),
          query=list(user="daniel")) %>% 
  httr::content()
```

```
## [[1]]
## [1] "mtcars.csv"
```



## Upload data 


```r
iris_path <- fs::path_join(c(tempdir(), "iris.csv"))

readr::write_csv2(iris, iris_path)

upload_result <- httr::PUT(plumber_url("/upload"),
                           query = list(user = "daniel"), 
                           body = list(data_file = httr::upload_file(iris_path)))

upload_result
```

```
## Response [http://localhost:8000/upload?user=daniel]
##   Date: 2020-07-06 20:03
##   Status: 200
##   Content-Type: application/json
##   Size: 6 B
```

```r
# curl -F "data_file=@iris.csv" "http://localhost:5000/upload?user=daniel"
```


```r
httr::GET(plumber_url("/list"),
          query=list(user="daniel")) %>% 
  httr::content()
```

```
## [[1]]
## [1] "iris.csv"
## 
## [[2]]
## [1] "mtcars.csv"
```


## Check structure


```r
httr::GET(plumber_url("/structure"),
          query=list(user="daniel", file="iris.csv")) %>% 
  httr::content()
```

```
## [[1]]
## [1] "Sepal.Length"
## 
## [[2]]
## [1] "Sepal.Width"
## 
## [[3]]
## [1] "Petal.Length"
## 
## [[4]]
## [1] "Petal.Width"
## 
## [[5]]
## [1] "Species"
```


### Remove data


```r
httr::PUT(plumber_url("/remove"),
                    query=list(user="daniel", file="iris.csv"))
```

```
## Response [http://localhost:8000/remove?user=daniel&file=iris.csv]
##   Date: 2020-07-06 20:03
##   Status: 200
##   Content-Type: application/json
##   Size: 6 B
```


```r
httr::GET(plumber_url("/list"),
          query=list(user="daniel")) %>% 
  httr::content()
```

```
## [[1]]
## [1] "mtcars.csv"
```



# Step 4: Build a frontend and add user authentication

Now, your users again need R (or the command line) to query your API.

So we need a web frontend for the users to use. And we need a way of authentication, instead of just passing the username as a parameter.

Steps:

1. Setup a second plumber process which serves HTML pages as a frontend. Use templates to construct the HTML.


```r
#* @get /process
#* @html
#* @param user
function(...) {
  # ...

  template <- htmltools::htmlTemplate("templates/process.html",
                                      user = user,
                                      file_name = file_name,
                                    ...)

  htmltools::renderDocument(template)
}
```


2. Handle user authentication via password and cookies. Use plumber filters to check and force login



```r
#* @get /login
#* @post /login
#* @param user
#* @param pwd
#* @html
function(req, res, user, pwd) {
  if (missing(user)) {
    template <- htmltools::htmlTemplate("templates/login.html",
                                        message = "Please login")
    htmltools::renderDocument(template)
  } else {
    # ...
    # on sucessful login
      session <- uuid::UUIDgenerate()
      res$setCookie("session", session)
      user_management[login$id, "session"] <<- session
      res$status <- 303 # redirect
      res$setHeader("Location", "/")
      res
      
    # ...
  }
}

#* @filter setuser
function(req){
  if (!is.null(req$cookies$session)) {
    username <- user_management %>% 
      dplyr::filter(session == req$cookies$session) %>% 
      dplyr::pull(username)
    
    req$username <- username
  } else {
    req$username <- character()
  }
  
  plumber::forward()
}
```



# Step 5: Coordinate multiple servers with docker-compose

Building, configuring and starting multiple docker containers is cumbersome on the command line.

You can orchestrate these with docker-compose

```
### docker-compose.yaml

version: '3'
services:
  data_pipeline:
    build: apps/data_pipeline/
    ports:
      - "8000:80"
  pipeline_frontend:
    build: apps/frontend/
    ports:
      - "80:80"
    environment:
      - PIPELINE_HOST=data_pipeline
      - PIPELINE_PORT=80

```

Build and run the containers

```
docker-compose build
docker-compose up
```



