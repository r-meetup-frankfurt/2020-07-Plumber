#!/bin/bash
exec Rscript -e "options('plumber.port'=80); pr <- plumber::plumb('$1'); pr\$run(host='0.0.0.0')"
