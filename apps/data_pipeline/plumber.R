#
# This is a Plumber API. In RStudio 1.2 or newer you can run the API by
# clicking the 'Run API' button above.
#
# In RStudio 1.1 or older, see the Plumber documentation for details
# on running the API.
#
# Find out more about building APIs with Plumber here:
#
#    https://www.rplumber.io/
#

library(plumber)
library(ggplot2)

source("processing.R")



#* @param user your username
#* @param file filename of data to process
#* @param variable column to group by
#* @param observable column to aggregate
#* @get /process
function(res, user, file, variable, observable){
  file_path = file.path("data", user, file)
  if (!file.exists(file_path)) {
    msg <- "The data file you've specified could not be found."
    res$status <- 400 # Bad request
    list(error=jsonlite::unbox(msg))
  } else {
    process_data(file_path, variable, observable)
  }
}



#* @param dataframe the dataframe as json array of dicts
#* @param x x-axis of the plot
#* @param y y-axis of the plot
#* @get /plot
#* @png
function(res, dataframe, x, y){
  dataframe <- jsonlite::fromJSON(dataframe, simplifyVector=TRUE)

  if (!all(c(x,y) %in% colnames(dataframe))) {
    msg <- "Columns not found in dataframe"
    res$status <- 400 # Bad request
    list(error=jsonlite::unbox(msg))
  } else {
    p <- generate_plot(dataframe, x, y)
    print(p)
  }
}




#* @param user
#* @put /upload
function(req, res, user) {
  multipart <- mime::parse_multipart(req)

  data_path = "data"
  target <- fs::path_join(c(data_path, 
                            user, 
                            multipart$data_file$name))
  print(multipart$data_file$name)
  fs::file_move(multipart$data_file$datapath, target)
  
  res$status <- 200
  "OK"
  
}


#* @param user
#* @param file
#* @put /remove
function(req, res, user, file) {
  file_path <- file.path("data", user, file)
  
  fs::file_delete(file_path)
  
  res$status <- 200
  "OK"
  
}


#* @param user
#* @get /list
function(res, user) {
  data_path = "data"
  target <- fs::path_join(c(data_path, 
                            user))
  fs::dir_ls(target, type="file") %>% 
    purrr::map_chr(basename)

}

#* @param user your username
#* @param file filename of data to process
#* @get /structure
function(res, user, file){
  file_path = file.path("data", user, file)
  if (!file.exists(file_path)) {
    msg <- "The data file you've specified could not be found."
    res$status <- 400 # Bad request
    list(error=jsonlite::unbox(msg))
  } else {
    suppressMessages(
      data <- readr::read_csv2(file_path)
    )
    colnames(data)
  }
}



# Filters ---------


#* Log some information about the incoming request
#* @filter logger
function(req){
  cat(as.character(Sys.time()), "-", 
      req$REQUEST_METHOD, req$PATH_INFO, "-", 
      req$HTTP_USER_AGENT, "@", req$REMOTE_ADDR, "\n")
  plumber::forward()
}
